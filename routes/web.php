<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/games', function () {
    

    $games = [
        ['name' => 'mine', 'version' => '1.123'],
        ['name' => 'dfs', 'version' => '1.433'],
        ['name' => 'sfda', 'version' => '1.445'],
        ['name' => 'adas', 'version' => '1.23'],
    ];

    $name = request('name');

    return view('index' ,[
        'index' =>  $games,
        'name' => $name
    ]);
});

Route::get('/api/games/{game}', function () {
    return view('show');
});

Route::post('/api/games', function () {
    return view('store');
});

Route::patch('/api/games/{game}', function () {
    return view('update');
});

Route::delete('/api/games/{game}', function () {
    return view('destroy');
});